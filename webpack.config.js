module.exports = {
  devtool: 'eval-source-map',
  entry:{ 
  main:'./src/chart.js',
  coin:'./src/coin.js',
  users:'./src/users.js'
},
  mode: 'development',
  module: {
    rules: [{
      exclude: /node_modules/,
      use: [{
        loader:'babel-loader',
      }],
      test: /\.jsx?$/
    }],  
  },
  output: {
    filename: '[name].bundle.js',
    path: __dirname + '/dist'
  }
}