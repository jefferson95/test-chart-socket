import Chart from "chart.js/auto";
import "regenerator-runtime/runtime";

const ctx = document.getElementById("chart").getContext('2d');

const data = [];
const labels = [];

const chart = initChart(ctx);

const getData = async _ => {
    const response = await fetch("https://rest.coinapi.io/v1/exchangerate/ETH/USD/history?period_id=1MIN&time_start=2021-01-01T00:00:00&time_end=2021-02-01T00:00:00", {
        headers: {
            "X-CoinAPI-Key": "7F908F03-026F-46EB-86FF-137A382A9A51",
        },
    });

    const responseData = await response.json();

    responseData.forEach(element => {
        data.push(element.rate_open);
        labels.push(element.time_period_start);
        chart.update();
    });

}

getData();


const sck = new WebSocket("wss://ws-sandbox.coinapi.io/v1/");
sck.addEventListener("open", () => {
    sck.send(`{
        "type": "hello",
        "apikey": "7F908F03-026F-46EB-86FF-137A382A9A51",
        "heartbeat": false,
        "subscribe_data_type": ["exrate"],
        "subscribe_filter_asset_id": [ "ETH"]
      }`);
});

sck.addEventListener("message", ev => {
    const dt = JSON.parse(ev.data);

    if (dt.asset_id_base === "ETH" && dt.asset_id_quote === "USD") {
         console.log(dt);
         
         data.push(dt.rate);
         labels.push(dt.time);
    }
    
    chart.update();
});

function initChart(context) {
    return new Chart (context, {
        type: "line",
        data: {
            labels,
            datasets: [
                {
                    label: "My first dataset",
                    data,
                    fill: false,
                    borderColor: "rgb(255,0,255)",
                    tension: 0.1,
                },
            ],
        },
        options: {
            scales: {
                x: {
                    beginAtZero: true,
                    max: 1000
                },
                y: {
                    beginsAtZero: true,
                    max: 5000
                },
            },
        },
    });
}