import { User } from "../../models/user";

export const getUsers = async _ => {
    const response = await fetch(`https://jsonplaceholder.typicode.com/users`);
    const data = await response.json();

    const users = data.map((element) => new User(element.id, element.email, element.name));

    // if the response.status equals 200 then return new User, otherwise return null
    return response.status === 200 ? users : null;

    // return fetch(`https://jsonplaceholder.typicode.com/users`)
    // .then(r => r.json)
    // .then(d => d.map((element) => new User(element.id, element.email, element.name)))
    // .catch(e => console.log(e));
}