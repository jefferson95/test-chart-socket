import Chart from "chart.js/auto";

let data = [];
let labels = [];

export function initChart(context) {
    return new Chart(context, {
      type: "line",
      data: {
        labels,
        datasets: [
          {
            label: "My First Dataset",
            data,
            fill: false,
            borderColor: "rgb(75, 192, 192)",
            tension: 0.1,
          },
        ],
      },
      options: {
        animation: false,
        scales: {
          x: {
            beginAtZero: true,
            max: 140,
          },
          y: {
            beginAtZero: true,
            max: 100,
          },
        },
      },
    });
};

  export const upChart = (chart, dt) => {
      data.push(+dt.value);
      labels.push(dt.time);
      chart.update();
  };