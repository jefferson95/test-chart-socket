
import { initChart } from "./functions/initChart";
import { upChart } from "./functions/initChart";

const ctx = document.getElementById("chart").getContext("2d");
const chart = initChart(ctx);

const sck = new WebSocket("ws://localhost:9000");

sck.addEventListener("message", ev => {
  const dt = JSON.parse(ev.data);
  console.log(ev.data);
  upChart(chart, dt);
});
